---
marp: true
#paginate: true
title: Cloud Engineering - Introduction
author: Alexander Hensels & Bram Verbruggen
keywords: thomasmore cloud engineering
theme: tm
_class:
  - invert
  - lead
---
# Cloud Engineering
**Introduction**


---

<!-- paginate: true -->
<!-- _header: Your teacher! -->
**Bram Verbruggen**
![bg right](img/2022-09-15-12-30-16.png)

</section>

---

**Why are we here, today?** 
[Link to report](https://services.google.com/fh/files/misc/2022_state_of_devops_report.pdf)


![bg left fit](img/2023-09-12-14-50-48.png)

---
<!-- _class: invert -->
<style scoped>
    section {
    background-image: none;
}
</style>

![bg fit](img/2023-09-12-15-07-50.png)

---
<!-- _class: invert -->
<style scoped>
    section {
    background-image: none;
}
</style>

![bg fit](img/2023-09-12-15-15-46.png)

---

>We found that relative to other clusters, the **Flowing** cluster focuses more on:
> - Loosely-coupled architectures
> - Providing flexibility to teams
> - Version control for infrastructure and application code
> - Continious integration and delivery (CI/CD)


---


* Cloud adoption :rocket:
* Delivery times :rocket:
* Failure rates ⬇️

![bg left fit](img/2023-09-12-14-50-48.png)

---


# [netdevops-survey 2020](https://dgarros.github.io/netdevops-survey/reports/2020)
Let's have look!

---

**Our mission**

* Prepare you to function **inside a modern DevOps-oriented environment**.
* Provide you with the necessary **infrastructure and cloud automation skills** to efficiently and effectively do your work. 

---
<!-- header: The course -->

#### Flipped classroom

- You will prepare the theory at home
- We will focus on the practical part in class

![bg left](img/flipped.png)

---
<!-- header: The course -->

#### Cisco DevNet Associate

- Provides a structure
- Some modules and labs
- We will skip a lot

![bg right](img/devasc.png)


---

#### Want the certificate & badge?

:point_right: Cisco Final exam, does not count for CE grade.

![bg right](img/devasc.png)

---
<!-- class: invert -->

<style scoped> 
   img {
    background-color: #ffff;
   }
</style>
Additionally, we will provide course materials and labs **on Gitlab**.

![bg left fit](img/gitlab.svg)

---

We believe in *practicing what we preach:*

***Course-as-Code***

![bg left fit](img/gitlab.svg)

---
<!-- class: _invert -->
<!-- header: "" -->

BTW: This slidedeck was written [as code...](https://gitlab.com/it-factory-thomas-more/cloud-engineering/course/ce-intro-slides)
```markdown
---

We believe in *practicing what we preach:*

***Course-as-Code***

![bg left](img/2022-09-15-16-44-23.png)

---
```
---

<!-- header: "" -->

...and was built using [a Gitlab CI/CD pipeline](https://gitlab.com/it-factory-thomas-more/cloud-engineering/course/ce-intro-slides).


---
<!-- _class: invert -->
<!-- header: Contents -->

- REST APIs using Python
- DevOps & Gitlab CI/CD
- Ansible
- Terraform
- Cisco RestCONF, NetConf and more...

---
<!-- header: The coins: first attempt -->

- **70% Permanent evaluation**
    - 2 Assignments **20%**
    - IaC integration project **50%**
- **30% during exam period**
    - Digital test on Canvas **30%**

![bg left:33%](img/2022-09-15-19-37-31.png)

---

<!-- header: The coins: second attempt -->

**100% during exam period**

- Oral exam about theory **30%**
- Individual assignments **70%**

![bg left:33%](img/2022-09-15-19-37-31.png)

---
<!-- header: "" -->
<!-- _class: invert -->
<style scoped>
    div {
      background-color: #00000070;

    }
</style>

# <div>Now, let's embark on this magical journey together!:unicorn: :mage: :fairy:</div>

![bg](img/2022-09-15-19-01-33.png)