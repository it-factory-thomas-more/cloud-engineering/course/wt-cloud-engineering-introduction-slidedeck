# 📊 Cloud Engineering Introduction Slidedeck

Course introduction slides for the Cloud Engineering course at Thomas More University for Applied Science.

You can view the live web version of the slidedeck or download the pdf version.
## [View slides in browser](https://wt-cloud-engineering-introduction-slidedeck-it-f-2a1b7aa4d191d3.gitlab.io/)
## [Download as PDF](https://wt-cloud-engineering-introduction-slidedeck-it-f-2a1b7aa4d191d3.gitlab.io/export/slides.pdf)
